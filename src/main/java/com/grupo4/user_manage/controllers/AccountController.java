package com.grupo4.user_manage.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.el.stream.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.grupo4.user_manage.exceptions.AccountNotFoundException;
import com.grupo4.user_manage.exceptions.InsufficientBalanceException;
import com.grupo4.user_manage.models.Account;
import com.grupo4.user_manage.models.AccountUpdatePayload;
import com.grupo4.user_manage.models.Product;
import com.grupo4.user_manage.models.PurchasePayload;
import com.grupo4.user_manage.repositories.AccountRepository;
import com.grupo4.user_manage.repositories.ProductRepository;

@RestController
public class AccountController extends Throwable {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	ProductRepository productRepository;

	@GetMapping("/accounts/{username}")
	Account getAccount(@PathVariable String username) {
		return accountRepository.findById(username).orElseThrow(
				() -> new AccountNotFoundException("No se encontro una cuenta con el username: " + username));
	}

	@PostMapping("/accounts")
	Account newAccount(@RequestBody Account account) {
		return accountRepository.save(account);
	}

	@PostMapping("/accounts/addBalance")
	Account addeBalanceAccount(@RequestBody AccountUpdatePayload payload) {
		Account account = accountRepository.findById(payload.getUsername()).get();
		System.out.println(payload);
		account.setBalance(account.getBalance() + payload.getNewBalance());

		return accountRepository.save(account);
	}

	@PostMapping("/buy")
	Account updateBalance(@RequestBody PurchasePayload payload) {

		Account account = accountRepository.findById(payload.getUsername()).get();
		List<Integer> skus = payload.getProducts();
		double currentBalance = account.getBalance();
		Product product;
		List<Product> products = new ArrayList<Product>();
		double total = 0;

		for (Integer integer : skus) {
			product = productRepository.findById(integer).get();
			;
			total += product.getPrice();
			products.add(product);
		}

		if (currentBalance <= total) {
			throw new InsufficientBalanceException("Insufficient balance");
		}
		addPurchasedItem(account, products);
		account.setBalance(account.getBalance() - total);

		return accountRepository.save(account);
	}

	void addPurchasedItem(Account account, List<Product> purchasedItems) {

		purchasedItems.addAll(account.getPurchasedItems());
		account.setPurchasedItems(purchasedItems);
	}
}
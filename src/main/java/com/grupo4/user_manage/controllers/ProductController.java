package com.grupo4.user_manage.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.grupo4.user_manage.exceptions.ProductNotFoundException;
import com.grupo4.user_manage.models.Product;
import com.grupo4.user_manage.repositories.ProductRepository;

@RestController
public class ProductController {

	@Autowired
	ProductRepository productRepository;
	
	@GetMapping("/products/all")
	List<Product> getProducts() {
		return productRepository.findAll();
	}
	
	@GetMapping("/products/{sku}")
	Product getProduct(@PathVariable Integer sku) {
		return productRepository.findById(sku).orElseThrow(
				() -> new ProductNotFoundException("No se encontro un product con el username: " + sku));
	}	
	
	@PostMapping("/products")
	Product newProduct(@RequestBody Product product) {
		return productRepository.save(product);
	}
	
}

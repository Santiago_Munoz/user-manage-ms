package com.grupo4.user_manage.repositories;
import com.grupo4.user_manage.models.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository <Account, String> { }
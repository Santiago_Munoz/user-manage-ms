package com.grupo4.user_manage.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.grupo4.user_manage.models.Product;

public interface ProductRepository extends MongoRepository<Product, Integer>{

}

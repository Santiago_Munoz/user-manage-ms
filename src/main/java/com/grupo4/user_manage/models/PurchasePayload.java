package com.grupo4.user_manage.models;

import java.util.ArrayList;
import java.util.List;

public class PurchasePayload {

	private String username;
	private List<Integer> products = new ArrayList<>();
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<Integer> getProducts() {
		return products;
	}
	public void setProducts(List<Integer> products) {
		this.products = products;
	}
	@Override
	public String toString() {
		return "PurchasePayload [username=" + username + ", products=" + products + "]";
	}
	
}

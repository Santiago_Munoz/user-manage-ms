package com.grupo4.user_manage.models;

import org.springframework.data.annotation.Id;

public class Product {

	@Id
	private int sku;
	private String description; 
	private double price;
	
	public Product(String description, double price) {
		this.description = description;
		this.price = price; 
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getSku() {
		return sku;
	}

	public void setSku(int sku) {
		this.sku = sku;
	}
}

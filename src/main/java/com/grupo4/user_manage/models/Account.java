package com.grupo4.user_manage.models;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Account {
    @Id
    private String username;
    private double balance;
    private List<Product> purchasedItems = new ArrayList<Product>();

    public Account(String username, double balance) {
        this.username = username;
        this.balance = balance;
    }
    public String getUsername() {
        return username;
    }
    public List<Product> getPurchasedItems() {
		return purchasedItems;
	}
	public void setPurchasedItems(List<Product> purchasedItems) {
		this.purchasedItems = purchasedItems;
	}
	public void setUsername(String username) {
        this.username = username;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
}
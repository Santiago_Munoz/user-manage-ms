package com.grupo4.user_manage.models;

public class AccountUpdatePayload {

	private String username;
	private double newBalance;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public double getNewBalance() {
		return newBalance;
	}
	public void setNewBalance(double newBalance) {
		this.newBalance = newBalance;
	}
	@Override
	public String toString() {
		return "AccountUpdatePayload [username=" + username + ", newBalance=" + newBalance + "]";
	}
	
}
